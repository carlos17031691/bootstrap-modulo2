# README #

Este repositorio contiene el proyecto final para la evaluación del modulo 1 de desarrollador Full Stack

### Contenido del proyecto ###

1. El archivo .gitignore para evitar hacer seguimiento de los cambios en el directorio node_modules.
2. El script dev sobre el package.json que debe inicializar el server lite-server
3. Las referencias css y js a bootstrap, jquery y popper utilizando referencias a los archivos correspondientes de la carpeta node_modules sobre la página principal (index.html) , en el body, un elemento jumbotron con el título de su proyecto.
4. Un container con 3 párrafos utilizando los elementos p y un título con descripción a cada uno contando las principales características del producto.
5. Dentro del container y separado de los párrafos previos, una lista de productos que incluya un título, descripción y un botón, esta vez utilizando el sistema de grillas con filas y columnas. Y todo esto, ¿es responsive?
6. El sistema de grillas donde ubicó los productos transformado a flex
7. Una sección de footer en la página principal que incluye información del comercio, redes sociales y algún otro detalle referido al sitio.
8. En el caso de la dirección comercial utilizando el componente address
9. En el archivo css creado, utiliza estilos que sean replicables para todos los productos del listado principal, de forma tal de cambiar ese diseño en el css y cambien todos los productos?
10. Las clases de control de alineación para ubicar los elementos del footer a la izquierda, centro y derecha

### Realizado por Carlos Torres ###